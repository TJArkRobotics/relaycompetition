================
# This is the demo code for NAO relay competition
================

## What is the code used for?

The code is used only for teams who want to compete in NAO relay games.

## Effect of the code

We test the code in our lab, it takes around 30s to finish a game.[20s if parameters optimized :)]

## Requirements:
+ NAO Version: V5
+ Naoqi Veresion: 2.1.0(or above)

## What is in the code?
1. Walking engine from B-Human code base and fine modified parameters.
2. A whistle recognizer.
3. Communication between two robots, enabling the relay process (i.e. when the first robot arrives, the second one starts).
4. A skill of walking straight by using inertial data. 

## IMPORTANT

1. For detailed usage, please refer to TJArkRelease.pdf.
2. This code can never be used unless according to our license. (refer to: License.txt) Once the code is used, it represents the agreement of lisence.
3. If you plan to use this code, please announce code usage by e-mailing to tjark.official@gmail.com (and to all teams if possible)

### WE WILL NOT GIVE ANY TECHNICAL SUPPORT!!! 
### WE WILL NOT BE RESPONSIBLE FOR ANY DAMAGE CAUSED BY THIS CODE!!!